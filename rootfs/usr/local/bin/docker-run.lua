#!/usr/bin/lua
local core = require('docker-core')

local function main()
  local profile = 'server'
  local run = 'su-exec core /usr/local/bin/ipfs'
  local folder = core.getenv('IPFS_PATH', '/var/lib/ipfs/.ipfs')

  core.update_user()
  core.run('mkdir -p %s', folder)
  core.chown(folder)

  if (core.test('! -f %s/config', folder)) then
    core.run('%s init --profile=%s', run, profile)
  end

  core.run('%s config Addresses.API /ip4/0.0.0.0/tcp/5001', run)
  core.run('%s config Addresses.Gateway /ip4/0.0.0.0/tcp/8080', run)
  -- core.run('%s config AutoNAT.ServiceMode enabled', run)
  -- core.run('%s config --json Experimental.QUIC true', run)
  -- core.run('%s config --json Discovery.MDNS.Enabled true', run)
  -- core.run('%s config --json Swarm.DisableNatPortMap false', run)
  -- core.run('%s config --json Swarm.EnableRelayHop true', run)
  -- core.run('%s config --json Swarm.EnableAutoRelay true', run)
  core.run('%s daemon --enable-gc --migrate --init-profile=%s', run, profile)
end

main()
