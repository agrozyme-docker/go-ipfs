#!/bin/bash
set -eux

function load_suffix() {
  local suffix=""
  local arch=$(apk --print-arch)

  case "${arch}" in
  x86_64)
    suffix="amd64"
    ;;
  x86)
    suffix="386"
    ;;
  aarch64)
    suffix="arm64"
    ;;
  armv7)
    suffix="arm"
    ;;
  *)
    suffix="${arch}"
    ;;
  esac

  echo "${suffix}"
}

function github_latest_version() {
  local repo="${1}"
  local version="${2:-}"

  if [[ "" == "${version}" ]]; then
    version=$(curl -s https://api.github.com/repos/${repo}/releases/latest | jq -r '.tag_name')
  fi

  echo "${version}"
}

function load_ipfs_version() {
  local repo="ipfs/go-ipfs"
  local version="${IPFS_VERSION:-}"
  github_latest_version "${repo}" "${version}"
}

function load_ipfs_source() {
  local version=$(load_ipfs_version)
  local suffix=$(load_suffix)
  local source="https://dist.ipfs.io/go-ipfs/${version}/go-ipfs_${version}_linux-${suffix}.tar.gz"
  echo "${source}"
}

function main() {
  local call="${1:-}"

  if [[ -z $(typeset -F "${call}") ]]; then
    return
  fi

  shift
  ${call} "$@"
}

main "$@"
