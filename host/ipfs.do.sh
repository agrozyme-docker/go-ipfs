#!/bin/bash
set +eux

function source_file() {
  echo "$(readlink -f ${BASH_SOURCE[0]})"
}

function source_path() {
  echo "$(dirname $(source_file))"
}

function setup_alias() {
  local run="$(source_file)"

  alias ipfs="${run} ipfs"
  alias ipfs-swarm-key-gen="${run} ipfs-swarm-key-gen"
}

function cli_command() {
  local image="docker.io/agrozyme/go-ipfs"
  local command="$(source_path)/docker.do.sh run_command -v ${PWD}:/var/lib/ipfs/.ipfs $@ ${image} "
  echo "${command}"
}

function ipfs() {
  local run="$(cli_command) ipfs $@"
  ${run}
}

function ipfs-swarm-key-gen() {
  local run="$(cli_command) ipfs-swarm-key-gen.sh $@"
  ${run}
}

function main() {
  local call=${1:-}

  if [[ -z $(typeset -F "${call}") ]]; then
    return
  fi

  shift
  ${call} "$@"
}

main "$@"
