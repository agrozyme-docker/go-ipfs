# Summary

Source: https://gitlab.com/agrozyme-docker/go-ipfs

Go implementation of IPFS, the InterPlanetary FileSystem

# Settings

Ports:

- `4001`: Swarm TCP; should be exposed to the public
- `5001`: Daemon API; must not be exposed publicly but to client services under you control
- `8080`: Web Gateway; can be exposed publicly with a proxy, e.g. as https://ipfs.example.org
- `8081`: Swarm Websockets; must be exposed publicly when the node is listening using the websocket transport (/ipX/.../tcp/8081/ws).

# Environment variables

- `IPFS_PATH`: default value is `/var/lib/ipfs/.ipfs`
- `IPFS_SWARM_KEY_FILE`
- `IPFS_SWARM_KEY`

Others see: https://github.com/ipfs/go-ipfs/blob/master/docs/environment-variables.md

# Commands

- [ipfs-swarm-key-gen](https://github.com/Kubuxu/go-ipfs-swarm-key-gen): https://github.com/ipfs/go-ipfs/issues/5767#issuecomment-612632099
