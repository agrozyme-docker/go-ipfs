ARG DOCKER_HUB_NAMESPACE=agrozyme
FROM "docker.io/${DOCKER_HUB_NAMESPACE}/alpine"
COPY rootfs /
ENV IPFS_PATH=/var/lib/ipfs/.ipfs
RUN set +e -ux && chmod +x /usr/local/bin/* && /usr/local/bin/docker-build.lua
EXPOSE 4001 5001 8080 8081
CMD ["/usr/local/bin/docker-run.lua"]
