#!/bin/bash
set -eux

function main() {
  local bin="./rootfs/usr/local/bin"
  local run="${bin}/docker-build.sh"
  chmod +x "${bin}"/*

  export IPFS_VERSION=$(${run} load_ipfs_version)
}

main "$@"
